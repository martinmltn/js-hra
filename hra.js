var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: 0,
            allowGravity:false,
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update,
    }
};

var player;
var jmeno = "Hrac";
var stars;
var bombs;
var foods;
var blackbirds;
var platforms;
var cursors;
var score = 0;
var gameOver = false;
var scoreText;
var highScore = 0;
var highScoreText;
var highScoreNote;
var restartText;
var lives = 3;
var livesText;
var gameOverText;
var level = 0;
var imunita = false;

if (localStorage.getItem(jmeno) == null) {
    highScore = 0;
}
else {
    highScore = localStorage.getItem(jmeno);
}

var game = new Phaser.Game(config);

function preload()
{
    this.load.image('bg', 'assets/bg.png');
    this.load.image('sady', 'assets/sady.png');
    this.load.image('ground', 'assets/platform.png');
    this.load.image('wall', 'assets/platformHor.png');
    this.load.image('star', 'assets/star.png');
    this.load.image('bomb', 'assets/bomb.png');
    this.load.image('food', 'assets/apple.png');
    this.load.image('kos', 'assets/kos.png');
    this.load.spritesheet('yoda', 'assets/yoda.png', { frameWidth: 32, frameHeight: 48 });
}

function create()
{
    //  prirazeni obrazku na pozadi
    this.add.image(400, 300, 'bg');


    //  tady je nastaveni platforem a jejich pridani do fyzicke group, aby jimi neproletaval ani hrac ani veci
    platforms = this.physics.add.staticGroup();

    //  tady jsou scale na okraje mapy aby pokryly cely obvod
    platforms.create(400,600, 'ground').setScale(2).refreshBody();
    platforms.create(400,0, 'ground').setScale(2).refreshBody();
    platforms.create(0, 600, 'wall').setScale(2).refreshBody();
    platforms.create(800, 600, 'wall').setScale(2).refreshBody();

    // nastaveni hrace, obrazku
    player = this.physics.add.sprite(400, 500, 'yoda').setOrigin(0.5,0.6);
    player.setSize(28,36);
    //player.setScale(.8);
    player.syncBounds = true;
    //  tady vytvarim animace pro chozeni a zastaveni hrace
    this.anims.create({
        key: 'walk',
        frames: this.anims.generateFrameNumbers('yoda', { start: 1, end: 3 }),
        frameRate: 10,
        repeat: -1
    });
    this.anims.create({
        key: 'walkUp',
        frames: this.anims.generateFrameNumbers('yoda', { start: 12, end: 15 }),
        frameRate: 10,
        repeat: -1
    });
    this.anims.create({
        key: 'left',
        frames: this.anims.generateFrameNumbers('yoda', { start: 4, end: 7 }),
        frameRate: 10,
        repeat: -1
    });
    this.anims.create({
        key: 'right',
        frames: this.anims.generateFrameNumbers('yoda', { start: 8, end: 11 }),
        frameRate: 10,
        repeat: -1
    });
    this.anims.create({
        key: 'turn',
        frames: [ { key: 'yoda', frame: 0 } ],
        frameRate: 10
    });
    //  udalosti pro input
    cursors = this.input.keyboard.createCursorKeys();
    //  nastaveni defaultni hvezdicky a pridani fyziky na jidloa bomby
    stars = this.physics.add.group({
        key: 'star',
        setXY: { x: Phaser.Math.Between(70, 730), y: Phaser.Math.Between(50, 530)}
    });

    foods = this.physics.add.group();
    bombs = this.physics.add.group();
    blackbirds = this.physics.add.group();

    // skore a zivoty
    scoreText = this.add.text(52, 10, 'Skóre: 0', { fontSize: '20px', fill: '#000' });
    livesText = this.add.text(580,10, 'Životů zbývá: '+lives, { fontSize: '20px', fill: '#000' });
    highScoreNote = this.add.text(200, 10, 'Rekord: '+highScore, { fontSize: '20px', fill: '#000' });


    //  Collidery na interakci mezi jednotlivymi objekty ve hre
    this.physics.add.collider(player, platforms);
    this.physics.add.collider(stars, platforms);
    this.physics.add.collider(bombs, platforms);
    this.physics.add.collider(foods, platforms);
    this.physics.add.collider(bombs, bombs);
    this.physics.add.collider(bombs, foods);


    //  tady se kontroluje prekryti hrace a hvezdicky pokud k nekomu dojde, tak aby se spustila funkce collect star
    this.physics.add.overlap(player, stars, collectStar, null, this);
    this.physics.add.collider(player, bombs, hitBomb, null, this);
    this.physics.add.collider(player, foods, hitFood, null, this);
    this.physics.add.overlap(player,blackbirds, collectKos, null, this);

    restart = this.input.keyboard.addKey("R".charCodeAt(0));
}

function update()
{
    if (gameOver){
        gameOverText = this.add.text(110, 180, 'Game Over! Tvé skóre: '+score, { fontSize: '38px', fill: '#000', backgroundColor:'white', align:'center' });
        highScoreText = this.add.text(110,280, 'Rekord: '+highScore, { fontSize: '32px', fill: '#000', backgroundColor:'white', align:'center' });
        restartText = this.add.text(110,380, "Pro restart hry stiskněte 'R'", { fontSize: '25px', fill: '#000', backgroundColor:'white', align:'left' });
    }
    if(cursors.up.isDown || cursors.down.isDown || cursors.left.isDown || cursors.right.isDown ){
        if(cursors.up.isDown){
            player.anims.play('walkUp', true);
            player.setVelocityY(-250);
        }else if(cursors.down.isDown){
            player.anims.play('walk', true);
            player.setVelocityY(250);
        }else{
            //player.anims.play('turn', true);
            player.setVelocityY(0);
        }
        if(cursors.left.isDown){
            player.setVelocityX(-250);
            player.anims.play('left', true);
        }else if(cursors.right.isDown){
            player.setVelocityX(250);
            player.anims.play('right', true);
        }else{
            //player.anims.play('turn', true);
            player.setVelocityX(0);
        }
    }else{
        player.anims.play('turn', true);
        player.setVelocityY(0);
        player.setVelocityX(0);
    }

    if(cursors.up.isDown && cursors.left.isDown){
        player.setVelocityX(-175);
        player.setVelocityY(-175);
    }
    if(cursors.up.isDown && cursors.right.isDown){
        player.setVelocityX(175);
        player.setVelocityY(-175);
    }
    if(cursors.down.isDown && cursors.left.isDown){
        player.setVelocityX(-175);
        player.setVelocityY(175);
    }
    if(cursors.down.isDown && cursors.right.isDown){
        player.setVelocityX(175);
        player.setVelocityY(175);
        player.anims.play('right', true);
    }



    if (lives == 2){
        player.setTint(0xFFFF00);
    }else if(lives == 1){
        player.setTint(0xFF9900);
    }else if(lives == 0){
        if(imunita==false){
            player.setTint(0xFF0000);
        }

        //player.anims.play('turn')
        this.physics.pause();
        localStorage.setItem(jmeno, highScore);
        gameOver = true;

    }else{
        player.setTint(0xFFFFFF);
    }

    if (restart.isDown) {
        localStorage.setItem(jmeno, highScore);
        location.reload();
    }

    if(imunita==true){
       // this.canvas.backgroundImage(300,400,'sady');
        player.setTint(0x0B5394);
    }
}

function collectStar (player, star)
{
    star.disableBody(true, true);

    //  přidava a aktualizuje skore
    score += 10;
    if(score >= highScore){
        highScore=score;
    }
    highScoreNote.setText('Rekord: ' + highScore);
    scoreText.setText('Skóre: ' + score);
    if (stars.countActive(true) === 0)
    {       //  tady je vyreseno spawnovani novych bomb, a hvezdicek s nahodne matematicky generovaonu pozici
        // zaroven je u bombicek osetreno, aby se nemohly obejvit primo ve hraci

        //stars.children.iterate(function (child) {
        //child.enableBody(true,Phaser.Math.Between(70, 730),Phaser.Math.Between(50, 530), true, true);

        var kosChance = Phaser.Math.Between(0,12);
        if(kosChance == 1){
            if(blackbirds.countActive(true)===0 && imunita==false){
                let q = (player.x < 400) ? Phaser.Math.Between(400, 730) : Phaser.Math.Between(70, 400);
                let v = (player.y < 300) ? Phaser.Math.Between(300, 530) : Phaser.Math.Between(50, 300);
                var kos = blackbirds.create(q,v,'kos');
                kos.allowGravity = false;
            }
        }
        if(imunita==true){
            imunita=false;
        }

        for (let i = 0; i < 1; i++){
            let x = (player.x < 400) ? Phaser.Math.Between(400, 730) : Phaser.Math.Between(70, 400);
            let y = (player.y < 300) ? Phaser.Math.Between(300, 530) : Phaser.Math.Between(50, 300);
            var bomb = bombs.create(x, y, 'bomb');
            bomb.setBounce(1);
            bomb.setCollideWorldBounds(true);
            bomb.setVelocity(Phaser.Math.Between(-200, 200), 100);
            bomb.allowGravity = false;
        }

        level += 0.25;
        for(let i = -1; i < level; i++){
            stars.create(Phaser.Math.Between(70, 730),Phaser.Math.Between(50, 530),'star');
        }

        var foodChance = Phaser.Math.Between(0,5);

        if(foodChance == 1){
            let x = (player.x < 400) ? Phaser.Math.Between(400, 730) : Phaser.Math.Between(70, 400);
            let y = (player.y < 300) ? Phaser.Math.Between(300, 530) : Phaser.Math.Between(50, 300);
            var food = foods.create(x,y,'food');
            food.setBounce(1);
            food.setCollideWorldBounds(true);
            food.setVelocity(Phaser.Math.Between(-200, 200), 40);
            food.allowGravity = false;
            food.setTint(0x00FF00);
        }

    }
}

function collectKos(player, kos) {
    imunita=true;
    kos.disableBody(true,true);
}

function hitFood (player, food)
{
    food.disableBody(true, true);
    lives += 1;
    livesText.setText('Životů zbývá: ' + lives);
}

function hitBomb (player, bomb)
{
    if(imunita===false){
        lives -= 1;
        bomb.disableBody(true, true);
        livesText.setText('Životů zbývá: ' + lives);
    }
}




